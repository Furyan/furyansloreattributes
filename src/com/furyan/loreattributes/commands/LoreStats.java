package com.furyan.loreattributes.commands;

import java.util.Arrays;
import java.util.List;

import com.furyan.util.LoreAttributeManager;

import PluginReference.ChatColor;
import PluginReference.MC_Command;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class LoreStats implements MC_Command
{
	private LoreAttributeManager _manager;

	public LoreStats(LoreAttributeManager manager)
	{
		_manager = manager;
	}

	@Override
	public List<String> getAliases()
	{
		return Arrays.asList(new String[]
		{ "ls" });
	}

	@Override
	public String getCommandName()
	{
		return "lorestats";
	}

	@Override
	public String getHelpLine(MC_Player player)
	{
		return ChatColor.DARK_PURPLE + "/lorestats - " + ChatColor.GOLD + "Displays your lore stats.";
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args)
	{
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args)
	{
		_manager.GetLoreStats(player);
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player)
	{
		return true;
	}
}
