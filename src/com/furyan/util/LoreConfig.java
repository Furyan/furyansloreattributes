package com.furyan.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import FuryansLoreAttributes.MyPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class LoreConfig {
	private File _configFile;
	private GsonBuilder _builder;

	@Expose
	public int PluginMajorVersion, PluginMinorVersion;

	@Expose
	public boolean DisplayDebugMessages;

	@Expose
	public HashMap<String, Object> Experience;

	@Expose
	public HashMap<String, Object> Health;

	@Expose
	public HashMap<String, Object> Regen;

	@Expose
	public HashMap<String, Object> AttackSpeed;

	@Expose
	public HashMap<String, Object> Damage;

	@Expose
	public HashMap<String, Object> Dodge;

	@Expose
	public HashMap<String, Object> CriticalChance;

	@Expose
	public HashMap<String, Object> CriticalDamage;

	@Expose
	public HashMap<String, Object> LifeSteal;

	@Expose
	public HashMap<String, Object> Armor;

	@Expose
	public HashMap<String, Object> Restriction;

	@Expose
	public HashMap<String, Object> BlockChance;

	@Expose
	public HashMap<String, Object> Block;

	@Expose
	public HashMap<String, Object> Phoenix;

	@Expose
	public HashMap<String, Object> NightSlayer;

	@Expose
	public HashMap<String, Object> Vorpal;

	public LoreConfig() {
		_builder = new GsonBuilder();
		_builder.setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation();

		_configFile = new File(MyPlugin.getPluginConfigDirectory(), "config.json");

		PluginMajorVersion = MyPlugin.MAJOR_VERSION;
		PluginMinorVersion = MyPlugin.MINOR_VERSION;
		Health = new HashMap<String, Object>();
		Regen = new HashMap<String, Object>();
		AttackSpeed = new HashMap<String, Object>();
		Damage = new HashMap<String, Object>();
		Dodge = new HashMap<String, Object>();
		CriticalChance = new HashMap<String, Object>();
		CriticalDamage = new HashMap<String, Object>();
		LifeSteal = new HashMap<String, Object>();
		Armor = new HashMap<String, Object>();
		Restriction = new HashMap<String, Object>();
		BlockChance = new HashMap<String, Object>();
		Block = new HashMap<String, Object>();
		Experience = new HashMap<String, Object>();
		Phoenix = new HashMap<String, Object>();
		NightSlayer = new HashMap<String, Object>();
		Vorpal = new HashMap<String, Object>();
	}

	public void LoadConfig() {
		Gson gson = _builder.create();

		if (!_configFile.exists()) {
			GenerateDefaultConfig();

		} else {
			System.out.println("=-=-= * Configuration file found! Loading it now...");
			FileInputStream fis;
			try {
				fis = new FileInputStream(_configFile);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}

				String json = sb.toString();

				LoreConfig config = gson.fromJson(json, LoreConfig.class);
				SetConfig(config);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void SaveConfig() {
		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try {
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Configuration file saved...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void GenerateDefaultConfig() {
		System.out.println("=-=-= * Configuration file not found! Creating default configuration file...");
		this.DisplayDebugMessages = false;
		this.Health.put("base-health", "20");
		this.Health.put("keyword", "health");
		this.Regen.put("keyword", "regen");
		this.AttackSpeed.put("enabled", false);
		this.AttackSpeed.put("display-message", false);
		this.AttackSpeed.put("message", "You're too tired to do damage.");
		this.AttackSpeed.put("base-delay", 2);
		this.AttackSpeed.put("keyword", "attack speed");
		this.Damage.put("keyword", "damage");
		this.Dodge.put("keyword", "evasion");
		this.CriticalChance.put("keyword", "critical chance");
		this.CriticalDamage.put("keyword", "critical damage");
		this.LifeSteal.put("keyword", "life steal");
		this.Armor.put("keyword", "armor");
		this.Restriction.put("keyword", "restriction");
		this.Restriction.put("display-message", false);
		this.Restriction.put("message", "You can't use the %itemname%");
		this.BlockChance.put("keyword", "block chance");
		this.Block.put("keyword", "block");
		this.Experience.put("keyword", "experience");
		this.NightSlayer.put("keyword", "nightslayer");
		this.Phoenix.put("enabled", false);
		this.Phoenix.put("display-message", false);
		this.Phoenix.put("message", "The phoenix was not with you this death.");
		this.Phoenix.put("base-delay", 1800);
		this.Phoenix.put("keyword", "phoenix");
		this.Vorpal.put("enabled", false);
		this.Vorpal.put("display-message", false);
		this.Vorpal.put("message", "Off with their head!");
		this.Vorpal.put("chance", "0.5");
		this.Vorpal.put("keyword", "vorpal");

		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try {
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Default configuration file created...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void SaveJsonToFile(File file, String json) throws IOException {
		FileOutputStream os;
		os = new FileOutputStream(file);
		os.write(json.getBytes());
		os.close();
	}

	private void SetConfig(LoreConfig config) {
		if (config.PluginMajorVersion < PluginMajorVersion || (config.PluginMajorVersion == PluginMajorVersion && config.PluginMinorVersion < PluginMinorVersion)) {
			System.out.println("=-=-= * Outdated config file found...");
			System.out.println("=-=-= * Adding new configuration options...");

			if (config.Health == null) {
				config.Health = new HashMap<String, Object>();
				config.Health.put("base-health", "20");
				config.Health.put("keyword", "health");
			}

			if (config.Regen == null) {
				config.Regen = new HashMap<String, Object>();
				config.Regen.put("keyword", "regen");
			}

			if (config.AttackSpeed == null) {
				config.AttackSpeed = new HashMap<String, Object>();
				config.AttackSpeed.put("enabled", false);
				config.AttackSpeed.put("display-message", false);
				config.AttackSpeed.put("message", "You're too tired to do damage.");
				config.AttackSpeed.put("base-delay", 2);
				config.AttackSpeed.put("keyword", "attack speed");
			}

			if (config.Damage == null) {
				config.Damage = new HashMap<String, Object>();
				config.Damage.put("keyword", "damage");
			}

			if (config.Dodge == null) {
				config.Dodge = new HashMap<String, Object>();
				config.Dodge.put("keyword", "evasion");
			}

			if (config.CriticalChance == null) {
				config.CriticalChance = new HashMap<String, Object>();
				config.CriticalChance.put("keyword", "critical chance");
			}

			if (config.CriticalDamage == null) {
				config.CriticalDamage = new HashMap<String, Object>();
				config.CriticalDamage.put("keyword", "critical damage");
			}

			if (config.LifeSteal == null) {
				config.LifeSteal = new HashMap<String, Object>();
				config.LifeSteal.put("keyword", "life steal");
			}

			if (config.Armor == null) {
				config.Armor = new HashMap<String, Object>();
				config.Armor.put("keyword", "armor");
			}

			if (config.Restriction == null) {
				config.Restriction = new HashMap<String, Object>();
				config.Restriction.put("keyword", "type");
				config.Restriction.put("display-message", false);
				config.Restriction.put("message", "You can't use the %itemname%");
			}

			if (config.BlockChance == null) {
				config.BlockChance = new HashMap<String, Object>();
				config.BlockChance.put("keyword", "block chance");
			}

			if (config.Block == null) {
				config.Block = new HashMap<String, Object>();
				config.Block.put("keyword", "block");
			}

			if (config.Experience == null) {
				config.Experience = new HashMap<String, Object>();
				config.Experience.put("keyword", "experience");
			}

			if (config.Phoenix == null) {
				config.Phoenix = new HashMap<String, Object>();
				config.Phoenix.put("enabled", false);
				config.Phoenix.put("display-message", false);
				config.Phoenix.put("message", "The phoenix was not with you this death.");
				config.Phoenix.put("base-delay", 1800);
				config.Phoenix.put("keyword", "phoenix");
			}

			if (config.NightSlayer == null) {
				config.NightSlayer = new HashMap<String, Object>();
				config.NightSlayer.put("keyword", "nightslayer");
			}

			if (config.Vorpal == null) {
				config.Vorpal = new HashMap<String, Object>();
				config.Vorpal.put("enabled", false);
				config.Vorpal.put("display-message", false);
				config.Vorpal.put("message", "Off with their head!");
				config.Vorpal.put("chance", "0.5");
				config.Vorpal.put("keyword", "vorpal");
			}
		}

		this.Health = config.Health;
		this.Regen = config.Regen;
		this.AttackSpeed = config.AttackSpeed;
		this.Damage = config.Damage;
		this.Dodge = config.Dodge;
		this.CriticalChance = config.CriticalChance;
		this.CriticalDamage = config.CriticalDamage;
		this.LifeSteal = config.LifeSteal;
		this.Armor = config.Armor;
		this.Restriction = config.Restriction;
		this.BlockChance = config.BlockChance;
		this.Block = config.Block;
		this.Experience = config.Experience;
		this.Phoenix = config.Phoenix;
		this.NightSlayer = config.NightSlayer;
		this.Vorpal = config.Vorpal;
		this.DisplayDebugMessages = config.DisplayDebugMessages;

		SaveConfig();
	}
}