package FuryansLoreAttributes;

import java.io.File;
import java.util.UUID;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_ContainerType;
import PluginReference.MC_DamageType;
import PluginReference.MC_Entity;
import PluginReference.MC_EventInfo;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

import com.furyan.loreattributes.commands.LoreStats;
import com.furyan.util.LoreAttributeManager;
import com.furyan.util.LoreConfig;
import com.furyan.util.StartMetrics;

/*
 * Health - Applied on login*, closing inventory*, respawn*, targeting (for mobs).
 * Damage, life steal, attack speed, critical chance, critical damage - Applied on attack.
 * Regen - Applied when player would normally regenerate health.
 * Item Restriction - Checked on inventory close, shooting bow / attacking, and crafting
 * Dodge, Armor - Applied on taking damage from another player or mob
 * Level Required - Checked on inventory close, shooting bow / attacking, and crafting
 */
public class MyPlugin extends PluginBase {
	public static int MAJOR_VERSION = 0;
	public static int MINOR_VERSION = 3;
	public static String VERSION_TAG = "BETA";
	public static String PLUGIN_NAME = "FuryansLoreAttributes";
	public static String PLUGIN_TITLE = "Furyan's Lore Attributes";
	public LoreConfig config = null;
	public File configFile = null;
	public LoreAttributeManager loreManager = null;
	public MC_Server Server = null;
	private MC_Player player;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.description = PLUGIN_TITLE + " v" + MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG + "";
		info.name = PLUGIN_NAME;
		info.version = MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG;

		return info;
	}

	public void onTick(int tickNumber) {
	}

	public void onStartup(MC_Server server) {
		Pl3xLibs.getScheduler().scheduleTask(PLUGIN_NAME, new StartMetrics(this.getPluginInfo()), 100);
		System.out.println("=-=-= " + PLUGIN_TITLE + " is starting up!");
		Server = server;

		System.out.println("=-=-= * Loading Configuration...");

		config = new LoreConfig();
		config.LoadConfig();

		loreManager = new LoreAttributeManager(this);

		Server.registerCommand(new LoreStats(loreManager));
	}

	public static File getPluginConfigDirectory() {
		File jar = new File(MyPlugin.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		File dir = new File(jar.getParent() + File.separator + PLUGIN_NAME + File.separator);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		return dir;
	}

	public void onShutdown() {
		System.out.println("=-=-= " + PLUGIN_NAME + " is shutting down!");
		config.SaveConfig();
	}

	// Check armor and adjust stats
	public void onContainerClosed(MC_Player player, MC_ContainerType containerType) {
		loreManager.ApplyHpBonus(player);
		player.setFoodRegenAmount(loreManager.getRegenBonus(player) + player.getFoodRegenAmount());
	}

	public void onAttemptEntityDamage(MC_Entity entity, MC_DamageType damageType, double damage, MC_EventInfo ei) {
		MC_Entity attacker = entity.getAttacker();

		if (ei.isCancelled) {
			return;
		}

		if (attacker != null) {
			DisplayDebugMessage("Initial Damage Amount: " + damage);
			if (loreManager.dodgedAttack(entity)) {
				ei.isCancelled = true;
				return;
			}

			if (attacker instanceof MC_Player) {
				if (loreManager.canAttack(attacker.getName())) {
					loreManager.addAttackCooldown(attacker);
				} else {
					if (!(boolean) config.AttackSpeed.get("display-message")) {
						ei.isCancelled = true;
						return;
					} else {
						((MC_Player) attacker).sendMessage((String) config.AttackSpeed.get("message"));
						ei.isCancelled = true;
						return;
					}
				}
			}

			damage += Math.max(0, loreManager.getDamageBonus(attacker) - loreManager.getArmorBonus(entity));
			DisplayDebugMessage("Damage no use of range: " + String.valueOf(damage));

			attacker.setHealth((float) Math.min(attacker.getMaxHealth(), attacker.getHealth() + Math.min(loreManager.getLifeSteal(attacker), damage)));
			DisplayDebugMessage("Adjusted Damage Amount: " + damage);
		}

		entity.setAdjustedIncomingDamage((float) damage);
	}

	public void onNonPlayerEntityDeath(MC_Entity victim, MC_Entity killer, MC_DamageType damageType) {
		DisplayDebugMessage("ON NONPLAYER ENTITY DEATH");

		if (killer instanceof MC_Player) {
			float xpBonus = loreManager.getExperienceBonus(killer);
			MC_Player player = ((MC_Player) killer);

			DisplayDebugMessage("XP Bonus: " + xpBonus);

			float totalXP = player.getExp() + xpBonus;

			DisplayDebugMessage("Adjusted XP" + totalXP);
			player.setExp(totalXP);
		}

	}

	public void onPlayerJoin(MC_Player player) {
		loreManager.ApplyHpBonus(player);
		player.setFoodRegenAmount(loreManager.getRegenBonus(player) + player.getFoodRegenAmount());
	}

	public void onPlayerRespawn(MC_Player player) {
		loreManager.ApplyHpBonus(player);
		player.setFoodRegenAmount(loreManager.getRegenBonus(player) + player.getFoodRegenAmount());
	}

	public void onPlayerLogout(String playerName, UUID uuid) {
	}

	public void DisplayDebugMessage(String message) {
		if (config.DisplayDebugMessages) {
			System.out.println("=-=DEBUG=-=" + message);
		}
	}
}
