# Copy and paste the text below as a template for entering bugs #

```
#!text

**Describe the issue:** [make it short and straight to the point]

**Steps to reproduce:**

1. [step 1]

2. [step 2]

3. [step 3]

4. and so on...


**Plugin Version:** [replace with version]

**Pl3xLibs Version:** [replace with version]

**Rainbow Version:** [replace with version]
```
